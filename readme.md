### Geometric Nonlinear Control MATLAB/Simulink Implementation

Written by Hoang Dinh Thinh (thinh@neuralmetrics.net), an extension to QuadSim (D. Hartman, K. Landis, M. Mehrer, S. Moreno, J. Kim).

For any error reporting, please email thinh@neuralmetrics.net. More documentation coming soon!